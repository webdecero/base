<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//Pages	
Route::resource('pages', 'PageController', [
    'parameters' => [
        'clave' => 'clave'
    ],
    'only' => [
        'edit',
        'update'
    ],
    'names' => [
//        'index' => 'manager.pages.index',
//        'create' => 'manager.pages.create',
//        'store' => 'manager.pages.store',
//        'show' => 'manager.pages.show',
        'edit' => 'manager.pages.edit',
        'update' => 'manager.pages.update',
//        'destroy' => 'manager.pages.destroy',
    ]
]);

