<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
//File Upload 

Route::post('fileUpload/load/ajax', ['as' => 'manager.fileUpload.load.ajax', 'uses' => 'FileUploadController@loadAjax']);

Route::post('fileUpload/load', ['as' => 'manager.fileUpload.load', 'uses' => 'FileUploadController@load']);
Route::resource('fileUpload', 'FileUploadController', [
    'only' => [
        'create'
    ],
    'names' => [
        'index' => 'manager.fileUpload.index',
        'create' => 'manager.fileUpload.create',
        'store' => 'manager.fileUpload.store',
        'show' => 'manager.fileUpload.show',
        'edit' => 'manager.fileUpload.edit',
        'update' => 'manager.fileUpload.update',
        'destroy' => 'manager.fileUpload.destroy',
    ]
]);




