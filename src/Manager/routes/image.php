<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//Image
Route::post('image/load', ['as' => 'manager.image.load', 'uses' => 'ImageController@load']);
Route::resource('image', 'ImageController', [
    'only' => [
        'create'
    ],
    'names' => [
        'index' => 'manager.image.index',
        'create' => 'manager.image.create',
        'store' => 'manager.image.store',
        'show' => 'manager.image.show',
        'edit' => 'manager.image.edit',
        'update' => 'manager.image.update',
        'destroy' => 'manager.image.destroy',
    ]
]);




