<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//User
Route::post('user/dataTable/{id?}/{excel?}', array('as' => 'manager.user.dataTable', 'uses' => 'UserDataTableController@dataTable'));
Route::get('user/{id}/delete', ['as' => 'manager.user.destroy', 'uses' => 'UserController@destroy']);
Route::resource('user', 'UserController', ['names' => [
        'index' => 'manager.user.index',
        'create' => 'manager.user.create',
        'store' => 'manager.user.store',
        'show' => 'manager.user.show',
        'edit' => 'manager.user.edit',
        'update' => 'manager.user.update',
//        'destroy' => 'manager.user.destroy',
]]);


