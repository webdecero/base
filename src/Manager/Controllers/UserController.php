<?php

namespace Webdecero\Base\Manager\Controllers;

//Providers

use Auth;
    

//Models
//Helpers and Class

use Webdecero\Base\Manager\Controllers\ManagerController;

class UserController extends ManagerController {

    public function index() {

        

        $data = array();

        $data['user'] = Auth::user();

        


        return view('baseViews::userPanel', $data);
    }

   

}
