<?php

namespace Webdecero\Base\Manager\Controllers;

//Providers
use Validator;
use Auth;
//Models
//Helpers and Class
use Illuminate\Http\Request;
use Webdecero\Base\Manager\Controllers\ManagerController;

class FileUploadController extends ManagerController {

    public function load(Request $request) {






        $input = $request->all();


        $input['disk'] = $request->input('disk', 'public');
        $input['prefix'] = $request->input('prefix', 'storage/');
        $input['folder'] = $request->input('folder', 'uploads');

        $rules = array(
            'file' => array('required')
        );

        $validator = Validator::make($input, $rules);



        if ($validator->fails() || !$request->file('file')->isValid()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
            ]);
        }



            $path = $request->file('file')->store($input['folder'], $input['disk']);







        if (!$path) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.incorrecta'),
                    ])->withInput($request->except('file'));
        }




        $this->data['path'] = asset($input['prefix'] . $path);



        return view('baseViews::archivoViewForm', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('baseViews::archivoAddForm', $this->data);
    }

    private function createNewFileStorageName($disk, $folder, $fileName) {


        $completeName = $folder . $fileName;



        $exists = $disk->exists($completeName);

        if ($exists) {
            $i = 1;
            while ($exists) {

                $completeName = $folder . $i . '_' . $fileName;

                $exists = $disk->exists($completeName);

                $i++;
            }
        }

        return $completeName;
    }

    public function loadAjax(Request $request) {
        $input = $request->all();

        $input['disk'] = $request->input('disk', 'public');
        $input['prefix'] = $request->input('prefix', 'storage/');
        $input['folder'] = $request->input('folder', 'uploads');
        $rules = array(
            'file' => array('required')
        );
        $validator = Validator::make($input, $rules);
        $response = [];
        if ($validator->fails()) {
            $response['success'] = false;
            $response['messages'] = $validator->messages()->all();
        } else if (!$request->file('file')->isValid()) {
            $response['success'] = false;
            $response['messages'] = 'Archivo no Válido';
        } else {

            $path = $request->file('file')->store($input['folder'], $input['disk']);

            if (!$path) {
                $response['success'] = false;
                $response['messages'] = trans('mensajes.operacion.incorrecta');
            } else {

                $httpPath = $input['prefix'] . $path;
                $response['success'] = true;
                $response['data'] = $httpPath;
            }
        }
        return response()->json($response);
    }

}
