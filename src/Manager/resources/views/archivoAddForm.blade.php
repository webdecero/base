@extends('baseViews::layouts.pageForm')


@section('sectionAction', 'Agregar')
@section('sectionName', 'Archivo')
@section('formAction', route('manager.fileUpload.load'))

@section('inputs')




<div class="form-group">
	<label>Cargar Archivo</label>



	<input type="file" class="form-control" name="file"  >
</div>


@endsection