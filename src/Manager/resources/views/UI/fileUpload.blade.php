<?php

$label = isset($item['label']) ? $item['label'] : '';
$name = isset($item['name']) ? $item['name'] : 'fileInput';
$required = isset($item['required']) && $item['required'] ? 'required' : '';
$value = isset($item['value']) ? $item['value'] : '';
?>

<div class="form-group">
    <label>{{$label}}</label>
    <div class="row campoInput">
        <div class="col-md-8 text-center">
            <input type="text" name="{{ $name }}"  class="hiddenImagePath form-control fileUploadName" value="{{$value}}"  {{$required}}>
        </div>
        <div class="col-md-4 text-center ">
            <button type="button" class="btn btn-primary btn-md openUploadFileModal" data-url="{{route('manager.fileUpload.load.ajax')}}">
                Cargar archivo
            </button>
        </div>
    </div>



</div>

