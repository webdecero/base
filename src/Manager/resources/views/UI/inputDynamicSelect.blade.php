<?php
$label = isset($item['label']) ? $item['label'] : '';
$name = isset($item['name']) ? $item['name'] : '';
$value = isset($item['value']) ? $item['value'] : '';
$required = isset($item['required']) && $item['required'] ? 'required' : '';
$postfix = isset($item['postfix']) ? $item['postfix'] . '[]' : '';
$prefix = isset($item['prefix']) ? $item['prefix'] : '';
$options = isset($item['options']) ? $item['options'] : [];

$helper = isset($item['helper']) ? $item['helper'] : '';


?>

<div class="form-group">
    <label>{{$label}} </label>
    <select name="{{$prefix}}{{$name}}{{$postfix}}" class="form-control" {{$required}} >
    	<option value=""> </option>
        @foreach($options as $key => $option)
    <option value="{{$option['_id']}}" @if($option['_id'] == $value) selected @endif>{{$option['_id']}}</option>
        @endforeach 

    </select>

    @if($helper)
    <p class="help-block">{{$helper}}</p>
    @endif
</div>

