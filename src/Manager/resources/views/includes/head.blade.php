<meta charset="utf-8">
<base href="{{ url('/') }}/" >

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Uriel Acosta">
<meta name="csrf-token" content="{!! csrf_token() !!}">

<link rel="shortcut icon" href="{{config('manager.base.manager.config.logo')}}">

<title>CRM | Manager</title>

<!-- Bootstrap Core CSS -->
<link type="text/css" href="admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link type="text/css" href="admin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- LIB CSS Opcionales que se remplazan en vistas-->
@stack('css-lib')

<link type="text/css" href="admin/dist/css/jquery-ui.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link type="text/css" href="admin/dist/css/sb-admin-2.css" rel="stylesheet">


<!-- FONT AWESOME -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<!-- User CSS -->


<link type="text/css" href="admin/dist/css/style.css" rel="stylesheet">


