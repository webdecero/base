@extends('baseViews::layouts.pageForm')


@section('content')


<div id="wrapper">

    @include('baseViews::includes.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-fw @yield('sectionIcon','fa-pencil')"></i> @yield('sectionName')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @yield('sectionAction') @yield('sectionName')
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">

                                <form role="form" action="@yield('formAction')" method="POST" enctype="multipart/form-data" class="validate" >



                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#contenido" aria-controls="contenido" role="tab" data-toggle="tab">Contenido</a></li>
                                        <li role="presentation"><a href="#seo" aria-controls="profile" role="tab" data-toggle="tab">Seo</a></li>

                                    </ul>


                                    <div class="panel panel-default">

                                        <div class="panel-body">


                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="contenido">



                                                    @yield('inputs')



                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="seo">




                                                    @include('baseViews::chunk.seo')





                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-default">Guardar</button>

                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->




                                </form>
                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


@endsection