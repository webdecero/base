<!DOCTYPE html>
<html lang="es" >
    <head>
        @include('baseViews::includes.head')

</head>

<body>


        @yield('content')


        @section('footer')
        <!-- Footer -->
        @include('baseViews::includes.footer')
        <!-- Fin de footer -->
        @show

        @include('baseViews::includes.mensajes') 


        @include('baseViews::includes.js')

		
    </body>
</html>