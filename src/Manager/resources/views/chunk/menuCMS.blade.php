<?php

$locale =isset($key) ? $key: 'es';
$native = isset($item['native']) ? $item['native'] : '';

?>

<!-- Listado de Opciones -->
<li>
    <a href="#">
        <i class="fa fa-info fa-fw"></i> CMS {{title_case($native)}}<span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level">


        @foreach(config('manager.pages') as $key => $item )

        <li>
            <a href="{{route('manager.pages.edit', ['clave' => $key, 'locale' => $locale ])}}"><i class="fa fa-edit fa-fw"></i> {{$item['menuTitle']}}</a>
        </li>

        @endforeach   



    </ul>    

</li>