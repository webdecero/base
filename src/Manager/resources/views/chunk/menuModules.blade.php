<?php
$label = isset($item['label']) ? $item['label'] : '';
$active = isset($item['active']) && $item['active'] ? true : false;
$icon = isset($item['icon']) ? $item['icon'] : 'fa fa-desktop';
$link = isset($item['link']) ? $item['link'] :  NULL;
$target = isset($item['target']) ? $item['target'] :  '_self';


if(!$link){
    $link = Route::has($key) ? route($key) : '#';
}


?>


@if($active)
<li>
    <a target="{{$target}}" href="{{$link}}"><i class="{{$icon}}"></i> {{$label}}</a>
</li>
@endif

