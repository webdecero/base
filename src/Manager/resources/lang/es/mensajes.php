<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return array(
    'alerta' => array(
        'titulo' => 'Lo sentimos, hubo un problema',
        'detecto' => 'Se detecto lo siguiente:',
    ),
    'mensaje' => array(
        'titulo' => 'Mensaje',
    ),
    'operacion' => array(
        'correcta' => 'Operación finalizada éxito',
        'incorrecta' => 'Operación Incorrecta, intente nuevamente',
        'error' => 'Operación Errónea, intente mas tarde',
        'noEncotrado' => 'Operación Incorrecta, elemento no existente',
    ),
    'route' => array(
        'notFound' => 'Ruta no encontrada.',
    ),
    'config' => array(
        'noEncotrado' => 'Elemento configuración no existente',
    ),
    'usuario' => array(
        'noEncontrado' => 'El usuario no ha sido encontrado',
        'yaRegistrado' => 'El usuario ya se encuentra registrado',
        'invalidEmail' => 'Email invalido',
        'YaValidado' => 'El usuario ya se encuentra validado, es necesario autenticar',
        'login' => 'Acceso correcto',
        'recuperar' => 'Se envío una nueva contraseña a su correo electrónico',
        'noLogin' => 'Es necesario iniciar sesión'
    ),
    'registro' => array(
        'exito' => 'Registro Correcto',
        'incompleto' => 'Registro incompleto',
        'continuar' => 'Es necesario finalizar Registro',
        'validacionEmail' => 'Un mensaje de validación ha sido enviado a tu correo electrónico. Revisa tu correo para continuar con el proceso de activación de tu cuenta.',
    ),
    'logout' => array(
        'finalizar' => 'La sesión caducó, pero puedes volver a ingresar ',
        'correcto' => 'Registro incompleto'
    ),
    'ie' => array(
        'titulo' => 'TU NAVEGADOR NO ES COMPATIBLE',
        'mensaje' => 'TE RECOMENDAMOS UTILIZAR LA ÚLTIMA VERSIÓN DE:'
    ),
    'session' => array(
        'TokenMismatchException' => 'Ha pasado mucho tiempo, es necesario refrescar la pagina, intentalo nuevamente.',
        'privilegios' => 'No tienes suficientes privilegios para acceder',
        'politica' => 'Es necesario aceptar política de privacidad',
    ),
    'paypal' => array(
        'failed' => 'Ocurrió un error al realizar el pago<br> No se realizó ningún cargo a su tarjeta.<br> Favor de intentar con otra tarjeta.<br>',
        'denied' => 'Cobro declinado<br>No se realizó ningún cargo a su tarjeta.<br>La operación fue declinada por su banco emisor.<br>Favor de intentar con otra tarjeta.',
        'approved' => '<h3>Gracias por tu donativo.</h3><p> Recibe más información sobre cómo con tu ayuda, elevamos la movilidad social en México. Regístrate y conoce más.</p>',
        'noRespuesta' => 'Ocurrió un error al realizar el pago.',
        'expired' => 'Ha caducado la página, es necesario pulsar el botón comprar nuevamente.',
        'canceled' => 'Operación Cancelada<br>No se realizó ningún cargo a su tarjeta.',
        'utilizado' => 'La referencia ha sido ocupada,  es necesario pulsar el botón comprar nuevamente.',
        'comprado' => 'El usuario ya cuenta con una compra realizada.',
        'vacio' => 'El carrito se enecuentra vacio',
        'incomplete' => 'Operación incompleta intentar nuevamente.',
    ),
    'webpay' => array(
        'error' => 'Ocurrió un error al realizar el pago<br> No se realizó ningún cargo a su tarjeta.<br> Favor de intentar con otra tarjeta.<br>',
        'denied' => 'Cobro declinado<br>No se realizó ningún cargo a su tarjeta.<br>La operación fue declinada por su banco emisor.<br>Favor de intentar con otra tarjeta.',
        'approved' => 'Cobro exitoso',
        'noRespuesta' => 'Ocurrió un error al realizar el pago.',
        'refresh' => 'Ha caducado la página, es necesario pulsar el botón donar nuevamente.',
        'utilizado' => 'La referencia ha sido ocupada,  es necesario pulsar el botón donar nuevamente.',
        'comprado' => 'El usuario ya cuenta con una compra realizada.',
        'codigoError' => 'El código  de descuento es Incorrecto.',
    ),
    
    'botones' => array(
        'enviar' => 'Enviar',
        'guardar' => 'Guardar',
        'cerrar' => 'Cerrar',
    ),
);
