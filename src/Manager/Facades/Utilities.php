<?php

namespace Webdecero\Base\Manager\Facades;

use \Illuminate\Support\Facades\Facade;


/**
 * @see \Webdecero\Base\Manager\Controllers\Utilities
 */
class Utilities extends Facade {

    protected static function getFacadeAccessor() {
        return 'Utilities';
    }

}
