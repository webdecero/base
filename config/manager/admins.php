<?php

$pages = [];

$pages['index'] = [
    'isSeo' => FALSE,
    'sectionName' => 'Administrador',
    'sections' => [
        'imagen' => [
            'element' => 'imageResize',
            'label' => 'Avatar',
            'required' => true,
            'w' => '400',
            'h' => '400',
            'resize' => 'widen',
            'path' => 'img/photos',
            'ratio' => '1 / 1',
        ],
        'email' => [
            'element' => 'inputText',
            'label' => 'Email',
            'type' => 'email',
            'required' => true,
        ],
        'password' => [
            'element' => 'inputText',
            'label' => 'Password',
            'type' => 'password',
            'required' => true,
        ],
        'nombre' => [
            'element' => 'inputText',
            'label' => 'Nombre',
            'required' => true,
        ],
        'apellido' => [
            'element' => 'inputText',
            'label' => 'Apellido',
            'required' => true,
        ],
        'descripcion' => [
            'element' => 'inputTextarea',
            'label' => 'Descripción',
        ],
        'genero' => [
            'element' => 'inputSelect',
            'label' => 'Género',
            'required' => true,
            'options' => [
                'hombre' => 'hombre',
                'mujer' => 'mujer'
            ],
        ],
        'rol' => [
            'element' => 'inputSelect',
            'label' => 'Rol',
            'required' => true,
            'options' => [
                'admin' => 'Administrador',
                'editor' => 'Editor'
            ],
        ],
        'status' => [
            'element' => 'inputSelect',
            'label' => 'Estatus',
            'required' => true,
            'options' => [
                'true' => 'Publicado',
                'false' => 'No publicado'
            ],
            'helper' => 'Si el estatus es no publicado no estara visible dentro del sitio'
        ],
    ]
];




return $pages;

