# Changelog

All notable changes to `Webdecero basewebdecero/base` will be documented in this file

## 1.0.0 - 2017-12-24

- initial release
